<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2009 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
namespace jrsy\help\crypt;

/**
 * Crypt 加密实现类
 */

class Crypt {

    /**
     * 加密
     * @param string $str    要加密的数据
     * @return bool|string   加密后的数据
     */
    static public function encrypt($str,$key) {
        $str = json_encode($str,JSON_UNESCAPED_UNICODE);
        $data = openssl_encrypt($str, 'AES-128-ECB',$key, OPENSSL_RAW_DATA);
        $data = base64_encode($data);
        return unpack('H*', $data)[1]; //转换16进制
    }

    /**
     * 解密
     * @param string $str    要解密的数据
     * @return string        解密后的数据
     */
    static public function decrypt($str,$key) {
        $str =  pack('H*', $str); //16进制base64
        $decrypted = openssl_decrypt(base64_decode($str), 'AES-128-ECB',$key, OPENSSL_RAW_DATA);
        return json_decode($decrypted,true);
    }

}
