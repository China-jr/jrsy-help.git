<?php


namespace jrsy\help\crypt;

class RsaCodeEnum{

    static $SUCCESS = 0;
    static $EREOR = 1;
    static $NOPUBLICFILE = 10001;
    static $NOPRIVATEFILE = 10002;
    static $NOPRIVATEKEY = 10003;
    static $NOPUBLICEKEY = 10004;
    static $DATAANALYSIS = 10005;

      static $OBJECT = [
          0=>'数据解析成功',
          1=>'未知错误',
          10001=>'公钥文件不存在',
          10002=>'私钥文件不存在',
          10003=>'密钥不存在',
          10004=>'公钥不存在',
          10005=>'数据解析错误',
      ];



}
